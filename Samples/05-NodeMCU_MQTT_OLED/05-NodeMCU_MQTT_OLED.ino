//#define SSD1306_128_64
#include <ESP8266WiFi.h>
#include <Adafruit_MQTT.h>
#include <Adafruit_MQTT_Client.h>
#include <Adafruit_SSD1306.h>

/************************* WiFi Access Point *********************************/
#define WLAN_SSID       "SSID"
#define WLAN_PASS       "PASS"

/************************* Adafruit.io Setup *********************************/

#define AIO_SERVER      "test.mosquitto.org"
#define AIO_SERVERPORT  1883                   // use 8883 for SSL
#define AIO_USERNAME    ""
#define AIO_KEY         ""

/************ Global State (you don't need to change this!) ******************/

// Create an ESP8266 WiFiClient class to connect to the MQTT server.
WiFiClient client;

// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);

/****************************** Feeds ***************************************/
Adafruit_MQTT_Subscribe sub_motion = Adafruit_MQTT_Subscribe(&mqtt, "/atelieroc/motion");
Adafruit_MQTT_Subscribe sub_temp = Adafruit_MQTT_Subscribe(&mqtt, "/atelieroc/temperature");

//------------------------------------------------------------------------------
#define OLED_RESET LED_BUILTIN
Adafruit_SSD1306 display(OLED_RESET);

void setup() {
  //-- Serial port -------------------------------------------------------------
  Serial.begin(9600);
  Serial.println(F("\nAtelier OC - Agora"));

  //-- MQTT --------------------------------------------------------------------
  mqtt.subscribe(&sub_motion);
  mqtt.subscribe(&sub_temp);

  //-- Afficheur ---------------------------------------------------------------
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
  display.display();

  // On ré-initialise l'affichage et on met prépare pour le texte
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);

  //-- Wifi --------------------------------------------------------------------
  display.print(F("Wifi: "));
  display.println(WLAN_SSID);
  display.display();
  
  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    display.print(".");
    display.display();
  }
  display.println();

  display.println(F("WiFi connected."));
  display.print(F("IP: "));
  display.println(WiFi.localIP());
  display.display();

  delay(1000);
}

void MQTT_connect();

bool hasMotion = false;
int temperature = 0;
unsigned int lastUpdate = 0;

void loop() {
  MQTT_connect();
  
  // Attente d'un message ...
  Adafruit_MQTT_Subscribe *subscription;
  while ((subscription = mqtt.readSubscription(100))) {
    if (subscription == &sub_motion) {
      if (strcmp((char *)sub_motion.lastread, "true") == 0) {
        hasMotion = true;
      } else {
        hasMotion = false;
      }
    } else if(subscription == &sub_temp) {
      temperature = atoi((char *)sub_temp.lastread);
    }
    lastUpdate = 0;
  }

  // Affichage de la température
  display.clearDisplay();
//  display.setTextSize(1);
//  display.setTextColor(WHITE);
  display.setCursor(0,0);
  
  display.print("Mouv: ");
  display.println(hasMotion ? "oui" : "non");
  
  display.print("Temp: ");
  display.print((float)(temperature / 100.));
  display.println(" C");

  display.print("Date: ");
  display.println(lastUpdate++);
  
  display.display();
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}

