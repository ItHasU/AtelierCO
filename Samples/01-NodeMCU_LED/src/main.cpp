#include <Arduino.h>

const int LED = D0;    // GPIO 16

void setup() {
  // Initialisation de pin comme un sortie
  pinMode(LED, OUTPUT);
  // Allumer la LED
  // Attention: Allumé sur état bas
  digitalWrite(LED, LOW);
}

void loop() {
  // Rien ici pour l'instant
}
