/************************* WiFi Access Point *********************************/
#define WLAN_SSID       "Wifi SSID"
#define WLAN_PASS       "Password"

/************************* Adafruit.io Setup *********************************/

#define AIO_SERVER      ""
#define AIO_SERVERPORT  1883                   // use 8883 for SSL
#define AIO_USERNAME    ""
#define AIO_KEY         ""
