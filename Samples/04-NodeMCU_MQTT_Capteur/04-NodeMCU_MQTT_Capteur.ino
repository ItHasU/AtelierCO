#include <ESP8266WiFi.h>
#include <Adafruit_MQTT.h>
#include <Adafruit_MQTT_Client.h>
#include <OneWire.h>
#include <DallasTemperature.h>

// See config_adafruit.sample.h
/************************* WiFi Access Point *********************************/
#define WLAN_SSID       "SSID"
#define WLAN_PASS       "PASS"

/************************* Adafruit.io Setup *********************************/

#define AIO_SERVER      "test.mosquitto.org"
#define AIO_SERVERPORT  1883                   // use 8883 for SSL
#define AIO_USERNAME    ""
#define AIO_KEY         ""

const int PIN_IR = D0;
const int PIN_DS = D4;

/************ Global State (you don't need to change this!) ******************/

// Create an ESP8266 WiFiClient class to connect to the MQTT server.
WiFiClient client;
// or... use WiFiFlientSecure for SSL
//WiFiClientSecure client;

// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);

/****************************** Feeds ***************************************/

Adafruit_MQTT_Publish pub_motion = Adafruit_MQTT_Publish(&mqtt, "/atelieroc/motion");
Adafruit_MQTT_Publish pub_temp = Adafruit_MQTT_Publish(&mqtt, "/atelieroc/temperature");

//-- Sensors -------------------------------------------------------------------

int count = 0;
bool hasMotion = false;
#define MOTION_THRESHOLD 3

OneWire oneWire(PIN_DS);
DallasTemperature sensors(&oneWire);

//------------------------------------------------------------------------------
//-- MAIN ----------------------------------------------------------------------
//------------------------------------------------------------------------------

// Bug workaround for Arduino 1.6.6, it seems to need a function declaration
// for some reason (only affects ESP8266, likely an arduino-builder bug).
void MQTT_connect();

void setup() {
  //-- Serial port -------------------------------------------------------------
  Serial.begin(9600);
  Serial.println(F("\nAtelier OC - Agora"));

  //-- Initialisation capteurs -------------------------------------------------
  // Capteur de mouvements
  pinMode(PIN_IR, INPUT_PULLDOWN_16);
  
  // Température
  sensors.begin();

  //-- Wifi --------------------------------------------------------------------
  Serial.print(F("Connecting to "));
  Serial.println(WLAN_SSID);

  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  Serial.println(F("WiFi connected."));
  Serial.print(F("IP address: "));
  Serial.println(WiFi.localIP());

  Serial.println(F("Going through main loop..."));
}

void loop() {
  // Ensure the connection to the MQTT server is alive (this will make the first
  // connection and automatically reconnect when disconnected).  See the MQTT_connect
  // function definition further below.
  MQTT_connect();

  //-- Motion ------------------------------------------------------------------
  if (digitalRead(PIN_IR)) {
    count++;
    if (count > MOTION_THRESHOLD) {
      hasMotion = true;
      count = MOTION_THRESHOLD;
    }
  } else {
    count--;
    if (count < 0) {
      hasMotion = false;
      count = 0;
    }
  }

  pub_motion.publish(hasMotion ? "true" : "false");

  //-- Temperature -------------------------------------------------------------
  sensors.requestTemperatures();
  int temp = sensors.getTempCByIndex(0) * 100;
  char buffer[20];
  sprintf(buffer, "%d", temp);
  pub_temp.publish(buffer);

  //-- Wait --------------------------------------------------------------------
  delay(1000);
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}

