#include <Arduino.h>
#include <OneWire.h>
#include <DallasTemperature.h>

// Initialisation des bibliothèques
OneWire oneWire(D4);
DallasTemperature sensors(&oneWire);

void setup(void)
{
  Serial.begin(9600);
  // Initialsation DallasTemperature
  sensors.begin();
}

void loop(void)
{
  // Lecture des températures
  sensors.requestTemperatures();
  // Affichage
  Serial.print("Temperature: ");
  Serial.print(sensors.getTempCByIndex(0));
  Serial.println("°C");
}
