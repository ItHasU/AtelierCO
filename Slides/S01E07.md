# S01E07

* Connexion au Wifi
* Envoi de données sur [Adafruit IO](http://io.adafruit.com)

---

## Connexion au wifi

* Connexion au point d'accès
* Récupération d'une IP automatique

---

## Connexion au wifi - code

```
#include <Arduino.h>
#include <ESP8266WiFi.h>
#define WLAN_SSID "WIFI_SSID"
#define WLAN_PASS "WIFI_PASSWORD"

void setup() {
  Serial.begin(9600);
  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500); Serial.print(".");
  }
  Serial.print(F("\nIP address: "));
  Serial.println(WiFi.localIP());
}

void loop() {}
```

---

## Envoi de données

* MQTT est un protocole de messagerie publish-subscribe basé sur le protocole TCP/IP. [Wikipedia]
  * Clé -> Message
  * ex: /home/temperature -> "22.5"
* [Adafruit IO](http://io.adafruit.com) : Plateforme (beta) pour les objets connectés API HTTP / MQTT
  * Création d'un compte Adafruit pour pouvoir accéder au service
  * Création d'un "Feed"
  * Récupération de la clé d'authentification
---

## Communication MQTT (Adafruit.io)

* Bibliothèque : Adafruit MQTT Library (1092)
* Example de projet sur le projet Gitlab (03-NodeMCU_AdafruitIO)
  * Adafruit_MQTT_Publish pour publier des données
  * Adafruit_MQTT_Subscribe pour recevoir des données

---

## Résultat

<img src="images/AdafruitIO_feed.png" alt="Adafruit.io Feed" width="70%">

---

## Aller plus loin

* Monter son propre serveur MQTT [Mosquitto](https://mosquitto.org/)
* Bibliothèque MQTT [PAHO](http://www.eclipse.org/paho/)
